#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import time
import numpy as np
import cv2
import re
from flask import Flask
from flask import request
import base64
from PIL import Image
import io
import json
import yaml
#print(sys.getdefaultencoding())

#配置相似度
if os.path.isfile("./config.yaml"):
    #查找是否有配置文件，从配置文件载入配置
    with open('./config.yaml') as f:
        s = yaml.load(f)
    similar = int(s['similar'])
else:
    #没有查找到配置文件，载入默认配置
    similar = 200 # 相似度,默认值200

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'mothion detection!'

@app.route('/motionapi/setparams', methods=['POST'])
def setparams():
    global similar
    execStatus = True
    message = ''
    if request.method == 'POST':
        try:
            param = int(request.form.get('similar'))
            if param == None:
                raise Exception
            similar = param
            try:
                data = {
                    'similar' : similar
                }
                f = open('./config.yaml', 'w')
                yaml.dump(data, f)
                f.close()
                execStatus = True
                message = '参数配置成功'
            except:
                execStatus = True
                message = '参数配置成功但写入配置文件失败'
        except:
            execStatus = False
            message = '请求参数不正确'
    else:
        execStatus = False
        message = '请使用POST方法提交配置'
    t = {}
    if execStatus == True:
        s = {
            'similar' : similar
        }
        t['status'] = 'success'
        t['message'] = message
        t['data'] = s
    else:
        t['status'] = 'failed'
        t['message'] = message
    return json.dumps(t,ensure_ascii=False)

        
        

@app.route('/motionapi/compare', methods=['POST'])
def compare():
    start = time.clock()
    global similar
    execStatus = True
    moveStatus = False
    message = ''
    if request.method == 'POST':
        try:
            frame1 = request.form.get('image1')
            frame2 = request.form.get('image2')
            if (frame1 == None or frame2 == None):
                raise Exception
            try:
                frame1 = re.sub('^data:image/.+;base64,', '', frame1)
                frame2 = re.sub('^data:image/.+;base64,', '', frame2)
                try:
                    frame1 = base64.b64decode(frame1)
                    frame2 = base64.b64decode(frame2)
                    frame1 = io.BytesIO(frame1)
                    frame2 = io.BytesIO(frame2)
                    try:
                        frame1 = Image.open(frame1)
                        frame2 = Image.open(frame2)
                        frame1 = np.array(frame1, np.uint8)
                        frame2 = np.array(frame2, np.uint8)
                        try:
                            one_frame = frame1
                            two_frame = frame2
                            one_frame = cv2.resize(frame1, (500, 500))
                            two_frame = cv2.resize(frame2, (500, 500))
                            #cv2.imwrite('1.jpg',one_frame)
                            try:
                                # 处理第一帧
                                gray_img1 = cv2.cvtColor(one_frame, cv2.COLOR_BGR2GRAY)
                                gray_img1 = cv2.GaussianBlur(gray_img1, (21, 21), 0)
                                # 处理第二帧
                                gray_img2 = cv2.cvtColor(two_frame, cv2.COLOR_BGR2GRAY)
                                gray_img2 = cv2.GaussianBlur(gray_img2, (21, 21), 0)
                                # 比对帧
                                try:
                                    img_delta = cv2.absdiff(gray_img1, gray_img2)
                                    thresh = cv2.threshold(img_delta, 25, 255, cv2.THRESH_BINARY)[1]
                                    thresh = cv2.dilate(thresh, None, iterations=2)
                                    image, contours, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                                    if contours:
                                        for c in contours:
                                            if cv2.contourArea(c) < similar: # 设置敏感度
                                                diffCount = cv2.contourArea(c)
                                                moveStatus = False
                                                message = '没有物体在移动'
                                                continue
                                            else:
                                                #diffFrame = cv2.drawContours(frame1, contours, -1, (0,0,255),1)
                                                diffCount = cv2.contourArea(c)
                                                moveStatus = True,
                                                message = '有物体在移动'
                                                break
                                    else:
                                        message = '两帧完全一样，不能够判断是否在移动'
                                    execStatus = True
                                except:
                                    execStatus = False
                                    message = '图片比对失败'
                            except:
                                execStatus = False
                                message = '对图片进行高斯模糊处理失败'
                        except:
                            execStatus = False
                            message = '压缩图片失败'
                    except:
                        execStatus = False
                        message = 'RGB图片转灰度图片失败'
                    #frame1 = cv2.imdecode(frame1,cv2.IMREAD_COLOR)
                    #frame2 = cv2.imdecode(frame2,cv2.IMREAD_COLOR)
                except:
                    execStatus = False
                    message = 'base64编码转图片失败'
            except:
                print('格式化base64编码失败')
                execStatus = False
                message = '格式化base64编码失败'
        except:
            execStatus = False
            message = '请求参数不正确'
    else:
        execStatus = False
        message = '请求方法不正确,请使用POST方法'
    #cv2.imwrite('3.jpg', diffFrame)
    t = {}
    if execStatus == True:
        s = {
            'similar' : similar,
            'diffCount' : diffCount,
            'moveStatus' : moveStatus
        }
        t['status'] = 'success'
        t['message'] = message
        t['execTime'] = str(time.clock() - start)
        t['data'] = s
    else:
        t['status'] = 'failed'
        t['message'] = message
        t['execTime'] = str(time.clock() - start)
    return json.dumps(t,ensure_ascii=False)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)